## Prerequisite
- PHP v.7.3
- MySQL 
- Redis 
- composer v.1

## How To Install

1. clone project
2. run `composer install`
3. create empty database
4. copy `.env.example` into `.env` , and adjust the mysql and redis config
5. run `php artisan migrate`
6. run `php -S localhost:8000 -t ./public` to serve locally on port 8000
7. download [this](https://www.getpostman.com/collections/89aa6c2315757d0aeba8) postman collection
8. simple ewallet is ready to test :)

## Note:
1. Use Bearer token Authorization for all API other than register
