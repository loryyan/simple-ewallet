<?php

namespace App\Http\Controllers;

use App\Models\AppServiceLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public $logData;
    public $request;
    public $user;

    private $startTime;
    private $endTime;

    const TYPE_INCOMING_REQUEST = 'incoming';
    const TYPE_OUTGOING_REQUEST = 'outgoing';

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->generateLog();
    }

    public function generateLog()
    {
        $this->user = Auth::user() ?? null;
        $this->startTime = microtime(true);
        $this->logData = [
            'request_id' => uniqid(),
            'method' => $this->request->method(),
            'host' => $this->request->ip(),
            'request_type' => $this->request->path(),
            'type' => self::TYPE_INCOMING_REQUEST,
            'header' => json_encode($this->request->header()),
            'request' => json_encode($this->request->all())
        ];
    }

    public function sendResponse(array $result, int $code): JsonResponse
    {
        $this->afterAction($result, $code);
        return response()->json($result, $code);
    }

    public function afterAction(array $result, int $code): bool
    {
        $this->endTime = microtime(true);   
        $duration = $this->endTime - $this->startTime;

        $resData = [
            'response' => json_encode($result),
            'res_http_code' => $code,
            'start_time' => $this->startTime,
            'end_time' => $this->endTime,
            'duration' => $duration,
            'user_id' => $this->user ? $this->user->id : null
        ];

        $this->logData = array_merge($this->logData, $resData);
        return AppServiceLog::create($this->logData) ? true : false;
    }
}
