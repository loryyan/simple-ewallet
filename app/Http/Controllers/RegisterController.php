<?php
   
namespace App\Http\Controllers;

use App\Constants\HttpStatusCode;
use App\Http\Controllers\Controller as BaseController;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Validator;
   
class RegisterController extends BaseController
{
    /**
     * Register user
     * 
     * @return JsonResponse
     */
    public function register(): JsonResponse
    {
        $validator = Validator::make($this->request->all(), [
            'username' => 'required|unique:user',
        ]);

        if ($validator->fails()){
            $error = $validator->errors()->first();
            if ($error == 'validation.unique') {
                $response['message'] = __('register.input.username.unique');
                return $this->sendResponse($response, HttpStatusCode::HTTP_CONFLICT);
            }
            $response['message'] = $error;
            return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);       
        }
   
        if ($user = User::register($this->request->all())) {
            $response['token'] = $user->api_token;
            return $this->sendResponse($response, HttpStatusCode::HTTP_CREATED);
        }

        $response['message'] = 'Bad Request';
        return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);

    }
}