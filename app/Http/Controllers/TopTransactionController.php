<?php
   
namespace App\Http\Controllers;
   
use App\Constants\HttpStatusCode;
use App\Http\Controllers\Controller as BaseController;
use App\Interfaces\UserTransactionInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
   
class TopTransactionController extends BaseController
{
    protected $userTrx;

    public function __construct(UserTransactionInterface $userTrx, Request $request)
    {
        $this->userTrx = $userTrx;
        parent::__construct($request);
    }

    /**
     * Shows top transactions for both credit and debit for the current user.
     *
     * @return JsonResponse
     */
    public function topTrxPerUser(): JsonResponse
    {
        $list = $this->userTrx->getTopTrxPerUser($this->user);
        return $this->sendResponse($list, HttpStatusCode::HTTP_OK);
    }

    /**
     * List overall top transactions per users by value
     *
     * @return JsonResponse
     */
    public function topUserTrx(): JsonResponse
    {
        $key = 'top-user-trx';
        if (app('redis')->exists($key)) {
            $list = json_decode(app('redis')->get($key), true);
        } else {
            $list = $this->userTrx->getTopUserTrx();
            app('redis')->set($key, json_encode($list));
        }
        return $this->sendResponse($list, HttpStatusCode::HTTP_OK);
    }

}