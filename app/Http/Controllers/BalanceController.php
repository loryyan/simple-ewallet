<?php
   
namespace App\Http\Controllers;
   
use App\Constants\HttpStatusCode;
use App\Http\Controllers\Controller as BaseController;
use App\Interfaces\UserTransactionInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;
   
class BalanceController extends BaseController
{
    protected $userTrx;

    public function __construct(UserTransactionInterface $userTrx, Request $request)
    {
        $this->userTrx = $userTrx;
        parent::__construct($request);
    }

    /**
     * Read current user balance
     *
     * @return JsonResponse
     */
    public function read(): JsonResponse
    {
        $response['balance'] = $this->user->balance;
        return $this->sendResponse($response, HttpStatusCode::HTTP_OK);
    }

    /**
     * Topup balance
     *
     * @return JsonResponse
     */
    public function topup(): JsonResponse
    {
        $validator = Validator::make($this->request->all(), [
            'amount' => 'required|numeric|gt:0|lt:10000000'
        ]);

        if ($validator->fails()){
            $error = $validator->errors()->first();
            if ($error != 'validation.required') {
                $response['message'] = __('balance.input.amount.invalid');
                return $this->sendResponse($response, HttpStatusCode::HTTP_CONFLICT);
            }
            $response['message'] = $error;
            return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);
        }
        DB::beginTransaction();
        $topup = $this->userTrx->saveTransactionTopup($this->user, $this->request->amount);
        if ($topup) {
            DB::commit();
            $response['message'] = __('balance.success.topup');
            return $this->sendResponse($response, HttpStatusCode::HTTP_NO_CONTENT);
        }
        $response['message'] = __('balance.failed.topup');
        return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);  
    }

    /**
     * Transfer balance to another user
     *
     * @return JsonResponse
     */
    public function transfer(): JsonResponse
    {
        $validator = Validator::make($this->request->all(), [
            'to_username' => 'required',
            'amount' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()){
            $response['message'] = $validator->errors()->first();
            return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);
        }

        $destination = User::where('username', $this->request->to_username)->first();
        if (!$destination || $this->user->id == $destination->id) {
            $response['message'] = __('validation.not_found.destination');
            return $this->sendResponse($response, HttpStatusCode::HTTP_NOT_FOUND);
        }

        if ($this->request->amount > $this->user->balance) {
            $response['message'] = __('balance.failed.insufficient');
            return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();
        $transfer = $this->userTrx->transfer($this->user, $destination, $this->request->amount);

        if ($transfer) {
            DB::commit();
            $response['message'] = __('balance.success.transfer');
            return $this->sendResponse($response, HttpStatusCode::HTTP_NO_CONTENT);
        }
        $response['message'] = __('balance.failed.transfer');
        return $this->sendResponse($response, HttpStatusCode::HTTP_BAD_REQUEST);
    }

}