<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppServiceLog extends Model
{
    protected $table = 'app_service_log';

    protected $guarded = [];  

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
