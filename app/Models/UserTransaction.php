<?php

namespace App\Models;

use App\Observer\UserTransactionObserver;
use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    use UserTransactionObserver;
    
    const TYPE_CREDIT = 'cr';
    const TYPE_DEBIT = 'db';
    const TRX_TYPE_TRANSFER = 'transfer';
    const TRX_TYPE_TOPUP = 'topup';

    protected $table = 'user_transaction';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
