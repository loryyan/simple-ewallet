<?php

namespace App\Observer;

use App\Jobs\SyncTopUserTrxJob;
use App\Models\UserTransaction;

trait UserTransactionObserver 
{
    protected static function boot()
    {
        parent::boot();

        static::created(function ($data){
            if ($data->type == UserTransaction::TYPE_DEBIT) {
                dispatch(new SyncTopUserTrxJob);
            }
        });
    }
}