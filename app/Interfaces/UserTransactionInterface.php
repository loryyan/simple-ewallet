<?php

namespace App\Interfaces;

use App\Models\User;

interface UserTransactionInterface {

    public function create(array $array): bool;

    public function transfer(User $source, User $destination, int $amount): bool;

    public function getTopTrxPerUser(User $user): array;

    public function getTopUserTrx(): array;


}