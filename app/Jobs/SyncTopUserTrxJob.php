<?php

namespace App\Jobs;

use App\Interfaces\UserTransactionInterface;

class SyncTopUserTrxJob extends Job
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $key = 'top-user-trx';
        $repo = app(UserTransactionInterface::class);
        $list = $repo->getTopUserTrx();
        app('redis')->set($key, json_encode($list));
    }
}
