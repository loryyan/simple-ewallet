<?php

namespace App\Repositories;

use App\Interfaces\UserTransactionInterface;
use App\Models\User;
use App\Models\UserTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserTransactionRepository implements UserTransactionInterface {

    /**
     * Create user transaction
     *
     * @param array $attrs
     * 
     * @return bool
     */
    public function create(array $attrs): bool
    {
        $transaction = new UserTransaction;
        $transaction->source_id      = $attrs['source_id'];
        $transaction->destination_id = $attrs['destination_id'];
        $transaction->amount         = $attrs['amount']; 
        $transaction->journal_id     = $attrs['journal_id']; 
        $transaction->trx_type       = $attrs['trx_type']; 
        $transaction->type           = $attrs['type'];
        
        return $transaction->save();
    }

    /**
     * Create user topup transaction
     *
     * @param User $user
     * @param int $amount
     * 
     * @return bool
     */
    public function saveTransactionTopup(User $user, int $amount): bool
    {
        $journalId = 'TOPUP-'.Carbon::now()->format('YmdHis');

        $transaction = [
            'source_id' => $user->id,
            'destination_id' => $user->id,
            'amount' => $amount,
            'journal_id' => $journalId,
            'trx_type' => UserTransaction::TRX_TYPE_TOPUP,
            'type' => UserTransaction::TYPE_CREDIT
        ];

        $user->balance += $amount;

        return ($this->create($transaction) && $user->save());
    }

    /**
     * Save user debit transaction record
     *
     * @param User $source
     * @param User $destination
     * @param int $amount
     * @param string $journalId
     * 
     * @return bool
     */
    private function saveTransactionDebit(User $source, User $destination, int $amount, string $journalId): bool
    {
        $transaction = [
            'source_id' => $source->id,
            'destination_id' => $destination->id,
            'amount' => -1 * abs($amount),
            'journal_id' => $journalId,
            'trx_type' => UserTransaction::TRX_TYPE_TRANSFER,
            'type' => UserTransaction::TYPE_DEBIT
        ];

        $source->balance -= $amount;

        return ($this->create($transaction) && $source->save());
    }

    /**
     * Save user credit transaction record
     *
     * @param User $source
     * @param User $destination
     * @param int $amount
     * @param string $journalId
     * 
     * @return bool
     */
    private function saveTransactionCredit(User $source, User $destination, int $amount, string $journalId): bool
    {
        $transaction = [
            'source_id' => $source->id,
            'destination_id' => $destination->id,
            'amount' => $amount,
            'journal_id' => $journalId,
            'trx_type' => UserTransaction::TRX_TYPE_TRANSFER,
            'type' => UserTransaction::TYPE_CREDIT
        ];

        $source->balance += $amount;

        return ($this->create($transaction) && $source->save());
    }

    /**
     * Create user transfer transaction records for both credit and debit 
     *
     * @param User $source
     * @param User $destination
     * @param int $amount
     * 
     * @return bool
     */
    public function transfer(User $source, User $destination, int $amount): bool
    {
        $journalId = 'TRANS-'.Carbon::now()->format('YmdHis');

        $debit = $this->saveTransactionDebit($source, $destination, $amount, $journalId);
        $credit = $this->saveTransactionCredit($destination, $source, $amount, $journalId);

        return ($debit && $credit);
    }

    /**
     * Get top 10 transaction for current user for both credit and debit
     *
     * @param User $user
     * 
     * @return array
     */
    public function getTopTrxPerUser(User $user): array
    {
        return UserTransaction::join('user', 'user.id', '=', 'user_transaction.destination_id')
            ->orderBy('amount', 'desc')
            ->where('source_id', $user->id)
            ->limit(10)
            ->get(['user.username', 'amount'])
            ->toArray();
    }

    /**
     * Get top 10 total debit transaction
     *
     * @return array
     */
    public function getTopUserTrx(): array
    {
        return UserTransaction::join('user', 'user.id', '=', 'user_transaction.source_id')
            ->where('type', UserTransaction::TYPE_DEBIT)
            ->orderBy(DB::raw('SUM(user_transaction.amount)'),'asc')
            ->groupBy('user.username')
            ->limit(10)
            ->get(['user.username', DB::raw('SUM(user_transaction.amount) as transacted_value')])
            ->toArray();
    }
}