<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('create_user', ['uses' => 'RegisterController@register']);

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('balance_read', ['uses' => 'BalanceController@read']);
    $router->post('balance_topup', ['uses' => 'BalanceController@topup']);
    // rate limit transfer for 2 trx every 1 min
    $router->group(['middleware' => 'throttle:2,1'], function () use ($router) {
        $router->post('transfer', ['uses' => 'BalanceController@transfer']);
    });
    $router->get('top_transactions_per_user', ['uses' => 'TopTransactionController@topTrxPerUser']);
    $router->get('top_users', ['uses' => 'TopTransactionController@topUserTrx']);

});