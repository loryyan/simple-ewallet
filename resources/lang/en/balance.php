<?php

return [

    'input' => [
        'amount' => [
            'invalid' => 'Invalid topup amount',
        ]
    ],
    'success' => [
        'topup' => 'Topup successful',
        'transfer' => 'Transfer success'
    ],
    'failed' => [
        'topup' => 'Topup Failed',
        'transfer' => 'Transfer Failed',
        'insufficient' => 'Insufficient balance',
    ]

];
