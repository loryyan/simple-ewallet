<?php

return [
    'required' => 'The :attribute field is required.',
    'not_found' => [
        'destination' => 'Destination user not found'
    ]
];
