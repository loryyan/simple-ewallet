<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppServiceLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_service_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('request_id');
            $table->string('method');
            $table->string('host');
            $table->string('request_type');
            $table->string('type');
            $table->text('header');
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->integer('res_http_code');
            $table->double('start_time')->default(0);
            $table->double('end_time')->default(0);
            $table->double('duration')->default(0);
            $table->timestamps();

            $table->index('user_id', 'user_id_index');
            $table->index('host', 'host_index');
            $table->index('request_type', 'request_type_index');
            $table->index('type', 'type_index');
            $table->index('res_http_code', 'res_http_code_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_service_log');
    }
}
