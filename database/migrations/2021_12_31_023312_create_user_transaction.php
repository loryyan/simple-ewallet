<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_transaction', function (Blueprint $table) {
            $table->id();
            $table->string('journal_id');
            $table->enum('trx_type', ['transfer', 'topup'])->nullable();
            $table->unsignedInteger('source_id');
            $table->unsignedInteger('destination_id');
            $table->enum('type', ['cr', 'db'])->default('db');
            $table->double('amount');
            $table->timestamps();

            $table->index('source_id', 'source_id_index');
            $table->index('destination_id', 'destination_id_index');
            $table->index('trx_type', 'trx_type_index');
            $table->index('type', 'type_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balance_log');
    }
}
